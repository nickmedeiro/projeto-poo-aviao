package Aviao;

public class Aviao_ClasseExcutiva_ClasseEconomica {
	
	private int fileira;
	private int assento;
	private int inicio;
	private int assentoLet_ClasseEconomica;
	private int qtdPass_ClasseEconomica;
	private int fileiraReser_ClasseExecutiva;
	
	public Aviao_ClasseExcutiva_ClasseEconomica (int fileira, int assento, int inicio){
		this.setFileira(fileira);
		this.setAssento(assento);
		this.setInicio(inicio);			
	}

	public int getFileira() {
		return fileira;
	}

	public void setFileira(int fileira) {
		this.fileira = fileira;
	}

	public int getAssento() {
		return assento;
	}

	public void setAssento(int assento) {
		this.assento = assento;
	}

	public int getInicio() {
		return inicio;
	}

	public void setInicio(int inicio) {
		this.inicio = inicio;
	}

public int getQtdPass_ClasseEconomica() {
		return qtdPass_ClasseEconomica;
	}

	public void setQtdPass_ClasseEconomica(int qtdPass_ClasseEconomica) {
		this.qtdPass_ClasseEconomica = qtdPass_ClasseEconomica;
	}

public int getAssentoLet_ClasseEconomica() {
		return assentoLet_ClasseEconomica;
	}

	public void setAssentoLet_ClasseEconomica(int assentoLet_ClasseEconomica) {
		this.assentoLet_ClasseEconomica = assentoLet_ClasseEconomica;
	}

public int getFileiraReser_ClasseExecutiva() {
		return fileiraReser_ClasseExecutiva;
	}

	public void setFileiraReser_ClasseExecutiva(int fileiraReser_ClasseExecutiva) {
		this.fileiraReser_ClasseExecutiva = fileiraReser_ClasseExecutiva;
	}

}